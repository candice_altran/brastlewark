package com.cseenyen.axa.brastlewark.data.network;

import com.cseenyen.axa.brastlewark.data.model.Brastlewark;
import com.cseenyen.axa.brastlewark.data.model.Gnome;

import java.util.List;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by cseenyen on 18/01/15.
 */
public interface ApiService {
    /**
     * List of gnomes
     */
    @GET("data.json")
    Observable<Brastlewark> getGnomes();

}
