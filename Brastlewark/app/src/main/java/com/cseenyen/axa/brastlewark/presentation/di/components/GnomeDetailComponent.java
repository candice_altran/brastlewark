package com.cseenyen.axa.brastlewark.presentation.di.components;

import com.cseenyen.axa.brastlewark.presentation.di.ActivityScope;
import com.cseenyen.axa.brastlewark.presentation.di.modules.GnomeDetailModule;
import com.cseenyen.axa.brastlewark.presentation.di.modules.GnomeModule;
import com.cseenyen.axa.brastlewark.presentation.ui.detail.GnomeDetailActivity;
import com.cseenyen.axa.brastlewark.presentation.ui.detail.GnomeDetailFragment;

import dagger.Component;
import dagger.Subcomponent;

/**
 * Created by candicealtran on 16/03/16.
 */

@ActivityScope
@Subcomponent(
        modules = {
                GnomeDetailModule.class
        }
)

public interface GnomeDetailComponent {

    void inject(GnomeDetailActivity activity);

    void inject(GnomeDetailFragment fragment);
}
