package com.cseenyen.axa.brastlewark.presentation.ui.list;

import com.cseenyen.axa.brastlewark.data.model.Gnome;
import com.cseenyen.axa.brastlewark.presentation.base.BaseLoadDataView;

import java.util.List;

/**
 * Created by candicealtran on 16/03/16.
 */
public interface GnomeListView extends BaseLoadDataView {
    /**
     * Render a list of gnomes in the UI
     *
     * @param gnomesList
     *              The collection of {@link Gnome} that will be rendered
     */
    void renderGnomeList(List<Gnome> gnomesList);
}
