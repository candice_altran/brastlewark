package com.cseenyen.axa.brastlewark.presentation.di.components;

import com.cseenyen.axa.brastlewark.presentation.di.ActivityScope;
import com.cseenyen.axa.brastlewark.presentation.di.modules.GnomeModule;
import com.cseenyen.axa.brastlewark.presentation.ui.list.GnomeListActivity;

import dagger.Subcomponent;

/**
 * Created by cseenyen on 12/03/16.
 */

@ActivityScope
@Subcomponent(
        modules = {
                GnomeModule.class
        }
)

public interface GnomeComponent {

    void inject(GnomeListActivity activity);

}
