package com.cseenyen.axa.brastlewark.presentation.ui.image;

/**
 * Created by candicealtran on 17/03/16.
 */
public interface FullScreenImageView {
    /**
     * Render the image
     */
    public void renderImage();
}
