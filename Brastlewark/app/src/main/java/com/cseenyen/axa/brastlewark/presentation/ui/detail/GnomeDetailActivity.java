package com.cseenyen.axa.brastlewark.presentation.ui.detail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.cseenyen.axa.brastlewark.BrastlewarkApplication;
import com.cseenyen.axa.brastlewark.R;
import com.cseenyen.axa.brastlewark.data.model.Gnome;
import com.cseenyen.axa.brastlewark.presentation.base.BaseActivity;
import com.cseenyen.axa.brastlewark.presentation.di.HasComponent;
import com.cseenyen.axa.brastlewark.presentation.di.components.ApplicationComponent;
import com.cseenyen.axa.brastlewark.presentation.di.components.GnomeDetailComponent;
import com.cseenyen.axa.brastlewark.presentation.di.modules.GnomeDetailModule;
import com.cseenyen.axa.brastlewark.presentation.di.modules.GnomeModule;
import com.cseenyen.axa.brastlewark.presentation.ui.image.FullScreenImageActivity;

/**
 * Created by candicealtran on 16/03/16.
 */
public class GnomeDetailActivity extends BaseActivity implements HasComponent<GnomeDetailComponent>,
        GnomeDetailFragment.onDisplayImageFullScreenListener {

    private static final String INTENT_EXTRA_PARAM_GNOME_ID = "com.cseenyen.axa.brastlewark.INTENT_PARAM_USER_ID";
    private static final String INSTANCE_STATE_PARAM_GNOME_ID = "com.cseenyen.axa.brastlewark.STATE_PARAM_USER_ID";

    private int gnomeId;
    private GnomeDetailComponent detailComponent;

    public static Intent getCallingIntent(Context context, int userId) {
        Intent callingIntent = new Intent(context, GnomeDetailActivity.class);
        callingIntent.putExtra(INTENT_EXTRA_PARAM_GNOME_ID, userId);
        return callingIntent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_layout);

        initializeActivity(savedInstanceState);
        setupActivityComponent();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (outState != null) {
            outState.putInt(INSTANCE_STATE_PARAM_GNOME_ID, this.gnomeId);
        }
        super.onSaveInstanceState(outState);
    }

    private void initializeActivity(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            this.gnomeId = getIntent().getIntExtra(INTENT_EXTRA_PARAM_GNOME_ID, -1);
            addFragment(R.id.fragmentContainer, new GnomeDetailFragment());
        } else {
            this.gnomeId = savedInstanceState.getInt(INSTANCE_STATE_PARAM_GNOME_ID);
        }
    }

    protected void setupActivityComponent() {
        detailComponent = BrastlewarkApplication.get(this)
                .getApplicationComponent()
                .plus(new GnomeDetailModule(this.gnomeId));
        detailComponent.inject(this);
    }

    @Override
    public GnomeDetailComponent getComponent() {
        return detailComponent;
    }

    @Override
    public void displayImageFullScreen(String url) {
        Intent intentToLaunch = FullScreenImageActivity.getCallingIntent(this, url);
        startActivity(intentToLaunch);
    }
}
