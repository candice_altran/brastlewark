package com.cseenyen.axa.brastlewark.presentation.base;

import android.content.Context;

/**
 * Created by cseenyen on 27/02/16.
 */
public interface BaseLoadDataView {

    /**
     * Get a {@link Context}.
     */
    Context context();

    /**
     * Show a view with a progress bar indicating a loading process.
     */
    void showLoading();

    /**
     * Hide a loading view.
     */
    void hideLoading();

    /**
     * Show a retry view in case of an error when retrieving data.
     */
    void showRetry();

    /**
     * Hide a retry view shown if there was an error when retrieving data.
     */
    void hideRetry();

    /**
     * Displays an error message
     *
     * @param message String to display
     */
    void displayErrorMessage(String message);
}
