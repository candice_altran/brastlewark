package com.cseenyen.axa.brastlewark.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by candicealtran on 16/03/16.
 */
public class Gnome {

    @SerializedName("id")
    private Integer id;
    @SerializedName("name")
    private String name;
    @SerializedName("thumbnail")
    private String thumbnail;
    @SerializedName("age")
    private Integer age;
    @SerializedName("weight")
    private Double weight;
    @SerializedName("height")
    private Double height;
    @SerializedName("hair_color")
    private String hairColor;
    @SerializedName("professions")
    private List<String> professions = new ArrayList<String>();
    @SerializedName("friends")
    private List<String> friends = new ArrayList<String>();

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The thumbnail
     */
    public String getThumbnail() {
        return thumbnail;
    }

    /**
     * @param thumbnail The thumbnail
     */
    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    /**
     * @return The age
     */
    public Integer getAge() {
        return age;
    }

    /**
     * @param age The age
     */
    public void setAge(Integer age) {
        this.age = age;
    }

    /**
     * @return The weight
     */
    public Double getWeight() {
        return weight;
    }

    /**
     * @param weight The weight
     */
    public void setWeight(Double weight) {
        this.weight = weight;
    }

    /**
     * @return The height
     */
    public Double getHeight() {
        return height;
    }

    /**
     * @param height The height
     */
    public void setHeight(Double height) {
        this.height = height;
    }

    /**
     * @return The hairColor
     */
    public String getHairColor() {
        return hairColor;
    }

    /**
     * @param hairColor The hair_color
     */
    public void setHairColor(String hairColor) {
        this.hairColor = hairColor;
    }

    /**
     * @return The professions
     */
    public List<String> getProfessions() {
        return professions;
    }

    /**
     * @param professions The professions
     */
    public void setProfessions(List<String> professions) {
        this.professions = professions;
    }

    /**
     * @return The friends
     */
    public List<String> getFriends() {
        return friends;
    }

    /**
     * @param friends The friends
     */
    public void setFriends(List<String> friends) {
        this.friends = friends;
    }

    public String getProfessionsForDisplay() {
        if(this.professions.isEmpty()) {
            return "Work?! No way!";
        }
        StringBuilder sb = new StringBuilder();
        for(String profession : this.professions) {
            sb.append(profession);
            sb.append("\n");
        }
        return sb.toString();
    }

    public String getFriendsForDisplay() {
        if(this.friends.isEmpty()) {
            return "I have no friend :(";
        }

        StringBuilder sb = new StringBuilder();
        for(String friend : this.friends) {
            sb.append(friend);
            sb.append("\n");
        }
        return sb.toString();
    }

}
