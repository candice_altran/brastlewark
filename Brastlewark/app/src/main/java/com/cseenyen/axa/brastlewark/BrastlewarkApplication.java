package com.cseenyen.axa.brastlewark;

import android.app.Application;
import android.content.Context;

import com.cseenyen.axa.brastlewark.presentation.di.components.ApplicationComponent;
import com.cseenyen.axa.brastlewark.presentation.di.components.DaggerApplicationComponent;
import com.cseenyen.axa.brastlewark.presentation.di.components.FullScreenImageComponent;
import com.cseenyen.axa.brastlewark.presentation.di.modules.ApplicationModule;
import com.cseenyen.axa.brastlewark.presentation.di.modules.FullScreenImageModule;

/**
 * Created by candicealtran on 16/03/16.
 */
public class BrastlewarkApplication extends Application {

    private ApplicationComponent appComponent;
    private FullScreenImageComponent fullScreenImageComponent;

    public static BrastlewarkApplication get(Context context) {
        return (BrastlewarkApplication) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.initializeInjector();
    }

    private void initializeInjector() {
        this.appComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return this.appComponent;
    }

    public FullScreenImageComponent createFullScreenImageComponent(FullScreenImageModule fullScreenImageModule) {
        fullScreenImageComponent = appComponent.plus(fullScreenImageModule);
        return fullScreenImageComponent;
    }

    public void releaseFullScreenImageComponent() {
        fullScreenImageComponent = null;
    }

}

