package com.cseenyen.axa.brastlewark.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by candicealtran on 16/03/16.
 */
public class Brastlewark {

    @SerializedName("Brastlewark")
    private List<Gnome> gnomes = new ArrayList<Gnome>();

    /**
     * @return The list of {@link Gnome}
     */
    public List<Gnome> getGnomes() {
        return gnomes;
    }

    /**
     *
     * @param gnomesList
     *              the list of gnomes
     */
    public void setBrastlewark(List<Gnome> gnomesList) {
        this.gnomes = gnomesList;
    }

}

