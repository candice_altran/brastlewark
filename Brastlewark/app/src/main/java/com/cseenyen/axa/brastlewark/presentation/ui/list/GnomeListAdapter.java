/**
 * Copyright (C) 2014 android10.org. All rights reserved.
 *
 * @author Fernando Cejas (the android10 coder)
 */
package com.cseenyen.axa.brastlewark.presentation.ui.list;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;

import com.cseenyen.axa.brastlewark.R;
import com.cseenyen.axa.brastlewark.data.model.Gnome;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Adapter that manages a list of {@link Gnome}.
 */
public class GnomeListAdapter extends RecyclerView.Adapter<GnomeListAdapter.UserViewHolder>
        implements Filterable {

    @Override
    public Filter getFilter() {
        return new UserFilter(this, gnomesList);
    }

    public interface OnItemClickListener {
        void onUserItemClicked(Gnome gnome);
    }

    private List<Gnome> gnomesList;
    private final ArrayList<Gnome> filteredGnomeList;
    private final LayoutInflater layoutInflater;

    private OnItemClickListener onItemClickListener;

    @Inject
    Picasso picasso;
    @Inject
    @Named("imageSizeInteger")
    Integer imageSize;

    @Inject
    public GnomeListAdapter(Context context) {
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.gnomesList = Collections.emptyList();
        this.filteredGnomeList = new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        return (this.filteredGnomeList != null) ? this.filteredGnomeList.size() : 0;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = this.layoutInflater.inflate(R.layout.gnome_item_layout, parent, false);
        return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, final int position) {
        final Gnome gnome = this.filteredGnomeList.get(position);
        this.picasso.load(gnome.getThumbnail())
                .placeholder(R.drawable.image_placeholder_small)
                .error(R.drawable.image_error_small)
                .resize(imageSize, imageSize)
                .centerCrop()
                .into(holder.gnomeImage);
        holder.gnomeName.setText(gnome.getName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onUserItemClicked(gnome);
                }
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setUsersCollection(List<Gnome> gnomesList) {
        this.validateUsersCollection(gnomesList);
        this.gnomesList = gnomesList;
        this.filteredGnomeList.clear();
        this.filteredGnomeList.addAll(gnomesList);
        this.notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private void validateUsersCollection(List<Gnome> gnomesList) {
        if (gnomesList == null) {
            throw new IllegalArgumentException("The list cannot be null");
        }
    }

    static class UserViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.gnome_image)
        ImageView gnomeImage;
        @Bind(R.id.gnome_name)
        TextView gnomeName;

        public UserViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private static class UserFilter extends Filter {

        private final GnomeListAdapter adapter;
        private final List<Gnome> originalList;
        private final List<Gnome> filteredList;

        private UserFilter(GnomeListAdapter adapter, List<Gnome> originalList) {
            super();
            this.adapter = adapter;
            this.originalList = new LinkedList<>(originalList);
            this.filteredList = new ArrayList<>();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filteredList.clear();
            final FilterResults results = new FilterResults();

            if (constraint.length() == 0) {
                filteredList.addAll(originalList);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();

                for (final Gnome user : originalList) {
                    if (user.getName().toLowerCase().trim().contains(filterPattern)) {
                        filteredList.add(user);
                    }
                }
            }
            results.values = filteredList;
            results.count = filteredList.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            adapter.filteredGnomeList.clear();
            List<Gnome> result = (ArrayList<Gnome>)results.values;
            adapter.filteredGnomeList.addAll(result);
            adapter.notifyDataSetChanged();
        }
    }
}
